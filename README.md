# Backend API for SahabatBagus Mobile Apps

## API documentations:

    http://localhost:8000/redoc 
    
## Project Setup

Install required packages:

    pip3 install -r requirements.txt


Initialize database:

    python3 manage.py makemigrations
    python3 manage.py migrate


## Fixtures

To load in sample data for all tables at once:

    python3 manage.py loaddata user.json


This will create an initial superuser account with the following credentials:

    admin
    S@habatB4gus

