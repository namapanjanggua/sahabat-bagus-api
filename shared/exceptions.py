from rest_framework import status
from rest_framework.exceptions import APIException
from rest_framework.views import exception_handler
from django.utils.translation import ugettext_lazy as _


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    if response is not None:
        try:
            if response.status_code == status.HTTP_400_BAD_REQUEST:
                if not response.data.get('detail'):
                    non_fields_errors = response.data.get("non_field_errors")
                    if non_fields_errors and isinstance(non_fields_errors, list):
                        non_fields_errors = non_fields_errors[0]
                    # change default validation error response body,
                    # make it so front end can easily parse it
                    resp = {
                        'error_code': response.status_code,
                        'detail': non_fields_errors if non_fields_errors else 'Bad Request (invalid request body)',
                        'moreInfo': response.data
                    }
                    response.data = resp

            # add error code if not provided!
            if not response.data.get('error_code'):
                response.data['error_code'] = response.status_code
        except Exception:
            pass

    return response


class CustomAPIException(APIException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    default_detail = _('Conflict.')
    default_code = '500'

    def __init__(self, detail: str, status_code: status = None, error_code: int =None, more_info=None,
                 developer_message: str=None):
        """

        :param `str` detail: detail error message
        :param `int` status_code: Http Status Code for response
        :param `int` error_code: Error code, could be different from http status code, default is same
        :param more_info: more error information
        :param developer_message: detail error message for developers only
        """
        if status_code:
            self.status_code = status_code
        if status_code is not None and error_code is None:
            error_code = status_code

        detail = {
            "detail": detail,
        }
        if error_code is not None:
            detail["error_code"] = error_code
        if more_info:
            detail["more_info"] = more_info
        if developer_message:
            detail["developer_message"] = developer_message
        super(CustomAPIException, self).__init__(detail=detail, code=error_code)


class ConflictApiEx(CustomAPIException):
    status_code = status.HTTP_409_CONFLICT
    default_detail = _('Conflict.')
    default_code = '409'


class UnprocessableEntityApiEx(CustomAPIException):
    status_code = status.HTTP_422_UNPROCESSABLE_ENTITY
    default_detail = _('Unprocessable Entity.')
    default_code = '422'


class PreconditionRequiredApiEx(CustomAPIException):
    status_code = status.HTTP_428_PRECONDITION_REQUIRED
    default_detail = _('Precondition Required.')
    default_code = '428'


class PreConditionFailedEx(CustomAPIException):
    status_code = status.HTTP_412_PRECONDITION_FAILED
    default_detail = _('Preconditions Failed.')
    default_code = '412'


class ExpectationFailedEx(CustomAPIException):
    status_code = status.HTTP_417_EXPECTATION_FAILED
    default_detail = _('Expectation Failed.')
    default_code = '417'


class FailedToConnectApiEx(CustomAPIException):
    status_code = status.HTTP_504_GATEWAY_TIMEOUT
    default_detail = _('Failed to connect External API.')
    default_code = '504'


class FailedReadResponsesApiEx(CustomAPIException):
    status_code = status.HTTP_505_HTTP_VERSION_NOT_SUPPORTED
    default_detail = _('Failed read Response.')
    default_code = '505'


class BadGatewayApiEx(CustomAPIException):
    status_code = status.HTTP_502_BAD_GATEWAY
    default_detail = _('Bad Gateway.')
    default_code = '502'


class RequestExternalFailedApiEx(CustomAPIException):
    status_code = status.HTTP_503_SERVICE_UNAVAILABLE
    default_detail = _('Failed on request to external service.')
    default_code = '503'