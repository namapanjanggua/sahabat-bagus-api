from dal import autocomplete
from django.contrib.auth.models import Group
from django.shortcuts import render
from django.utils.timezone import now
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status, viewsets, permissions
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework_jwt.serializers import JSONWebTokenSerializer
from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.utils import jwt_response_payload_handler
from rest_framework_jwt.views import ObtainJSONWebToken, RefreshJSONWebToken, VerifyJSONWebToken

from auth_token import serializers
from auth_token.models import User
from auth_token.permissions import IsSelf
from shared.exceptions import ConflictApiEx


class LoginJSONWebToken(ObtainJSONWebToken):
    serializer_class = JSONWebTokenSerializer

    @swagger_auto_schema(
        security=[],
        operation_id='auth_login',
        responses={
            status.HTTP_200_OK: serializers.LoginJWTResponseSerializer(),
            status.HTTP_400_BAD_REQUEST: "Unable to login with provided credential (wrong email or password).",
            status.HTTP_409_CONFLICT: "(Conflict) User account is not active, user should verify his/her email address."
        },
    )
    def post(self, request, *args, **kwargs):
        """
        API View that receives a POST with a user's username and password.

        Returns a JSON Web Token that can be used for authenticated requests.
        """
        # return super(LoginJSONWebToken, self).post(request, *args, **kwargs)

        # for login response creation, see: services_jwt.jwt_response_payload_handler

        # validate user is active (email already confirmed)
        # if not, raised 409 conflict
        #   unable to override serializer validate method, all error will return 400,
        #   so we force to validate user.is_active here
        email = request.data.get('username')
        if email and User.objects.filter(email=email, is_active=False).exists():
            raise ConflictApiEx(
                "User account is not active, please verify your email address or contact our support")

        # workaround to get GENERAL error response if request invalid!
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = serializer.object.get('user') or request.user

        token = serializer.object.get('token')
        response_data = jwt_response_payload_handler(token, user, request)
        response = Response(response_data)
        if api_settings.JWT_AUTH_COOKIE:
            expiration = (now() +
                          api_settings.JWT_EXPIRATION_DELTA)
            response.set_cookie(api_settings.JWT_AUTH_COOKIE,
                                token,
                                expires=expiration,
                                httponly=True)
        return response


class RefreshJWT(RefreshJSONWebToken):

    @swagger_auto_schema(
        operation_id='refresh_token',
    )
    def post(self, request, *args, **kwargs):
        """
        API View that returns a refreshed token (with new expiration) based on
        existing token

        If 'orig_iat' field (original issued-at-time) is found, will first check
        if it's within expiration window, then copy it to the new token
        """
        # workaround to get GENERAL error response if request invalid!
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return super(RefreshJWT, self).post(request, *args, **kwargs)


class VerifyJWT(VerifyJSONWebToken):

    @swagger_auto_schema(
        operation_id='verify_token',
    )
    def post(self, request, *args, **kwargs):
        """
        API View that checks the veracity of a token, returning the token if it
        is valid.
        """
        # workaround to get GENERAL error response if request invalid!
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return super(VerifyJWT, self).post(request, *args, **kwargs)


SECRET_KEY = "sahabatBagus44553225Auth5566ffds"


class UserProfileViewSet(viewsets.GenericViewSet):

    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = serializers.UserDetailsSerializer
    permission_classes = (permissions.IsAuthenticated, IsSelf,)

    # /users/profile
    @swagger_auto_schema(
        # methods=['get', ],
        operation_id='users_profile_read',
        responses={status.HTTP_200_OK: serializers.UserDetailsSerializer()}
    )
    # so documentation didn't show query string pagination, we set this like a get detail route
    @action(detail=True, methods=['get', ])
    def get_profile(self, request, *args, **kwargs):
        """
        Get Current User Profile, based on user credential in Authorization header
        """
        context = {"request": request}
        serializer = serializers.UserDetailsSerializer(request.user, context=context)
        return Response(serializer.data)

    @swagger_auto_schema(
        operation_id='users_profile_update',
        request_body=serializers.UserDetailsSerializer(),
        responses={status.HTTP_200_OK: serializers.UserDetailsSerializer()}
    )
    def update_profile(self, request, *args, **kwargs):
        # return Response({}, status.HTTP_200_OK)
        self.serializer_class = serializers.UserDetailsSerializer
        # return super(UserViewSet, self).update(request, *args, **kwargs)
        partial = kwargs.pop('partial', False)
        instance = request.user
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        # register celery task to update member to Central (KG) MyValue Service
        instance.refresh_from_db()
        # tasks.register_task_send_member(instance)

        return Response(serializer.data, status.HTTP_200_OK)


class GroupsAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Group.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs