from django.conf.urls import url
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from auth_token import views

urlpatterns = [
    url(r'^auth/login/$', views.LoginJSONWebToken.as_view(), name='auth-login'),
    url(r'^auth/refresh-token/$', views.RefreshJWT.as_view(), name='auth-token-refresh'),
    url(r'^auth/verify-token/$', views.VerifyJWT.as_view(), name='auth-token-verify'),
    url(r'^users/profile/$', views.UserProfileViewSet.as_view(
        actions={
            "get": "get_profile",
            "put": "update_profile",
        },
    ), name='user-profile'),
    url(r'^group-autocomplete/$', views.GroupsAutocomplete.as_view(),
        name='group-autocomplete'),
]

urlpatterns = format_suffix_patterns(urlpatterns)

# http://www.django-rest-framework.org/tutorial/6-viewsets-and-routers/
auth_router = routers.DefaultRouter()