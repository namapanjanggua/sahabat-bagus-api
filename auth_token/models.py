from django.contrib.auth.models import AbstractUser
from django.db import models

from sahabatbagus import settings


class User(AbstractUser):
    # mobile_phone_number = models.CharField(max_length=20, blank=True)
    # referral = models.ForeignKey('self', blank=True, null=True, related_name='referral', on_delete=models.PROTECT)
    referral_code = models.CharField(max_length=20, unique=True, null=True, blank=True,)
    referral = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.PROTECT)
