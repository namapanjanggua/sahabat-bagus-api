from rest_framework import serializers
from rest_framework.reverse import reverse

from auth_token import models


class UserDetailsSerializer(serializers.HyperlinkedModelSerializer):

    url = serializers.SerializerMethodField(read_only=True)

    def get_url(self, obj):
        return reverse('user-profile', request=self.context.get('request'))

    class Meta:
        model = models.User
        exclude = ('username',  'is_staff', 'date_joined',  # 'is_active',
                   'password', 'is_superuser', 'groups', 'user_permissions',
                   'last_login')


class LoginJWTResponseSerializer(serializers.Serializer):

    user = UserDetailsSerializer()
    token = serializers.CharField()