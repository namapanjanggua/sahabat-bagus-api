from dal import autocomplete
from django.contrib.auth.forms import UserChangeForm, UsernameField
from django.contrib.auth.models import Group
from django.forms import ModelMultipleChoiceField

from auth_token import models


class UserForm(UserChangeForm):
    groups = ModelMultipleChoiceField(
        queryset=Group.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(
            url='group-autocomplete',
        ))

    class Meta:
        model = models.User
        fields = '__all__'
        field_classes = {'username': UsernameField}