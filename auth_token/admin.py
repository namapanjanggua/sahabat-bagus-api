from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from auth_token import forms, models
from django.contrib.auth.models import Group
from django.utils.translation import gettext_lazy as _


class UserSahabatBagusAdmin(UserAdmin):
    form = forms.UserForm

    list_display = ('username', 'email', 'first_name', 'last_name', 'is_active', 'is_staff')
    fieldsets = (
        (_("Main Info"), {'fields': ('username', 'password', 'is_active', 'is_staff', 'is_superuser',)}),
        (_('Personal info'), {
            'fields': (
                'first_name', 'last_name', 'email', 'referral'
            )
        }),
        (_('Permissions'), {'fields': ('groups', 'user_permissions', )}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    # readonly_fields = ('', )

    # change_form_template = "entities/user_change_form.html"

    def has_delete_permission(self, request, obj=None):
        return False


# Globally disable delete selected
admin.site.disable_action('delete_selected')

admin.site.register(models.User, UserSahabatBagusAdmin)
admin.site.unregister(Group)
